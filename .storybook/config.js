import { configure, addDecorator } from "@storybook/react"

import { withOptions } from "@storybook/addon-options"
import { withInfo } from "@storybook/addon-info"
import { withKnobs } from "@storybook/addon-knobs"
import { withNotes } from "@storybook/addon-notes"

const req = require.context("../src/stories", true, /\.stories\.js$/)

function loadStories() {
	req.keys().forEach(filename => req(filename))
}

addDecorator(withInfo)
addDecorator(withNotes)
addDecorator(withKnobs)

addDecorator(
	withOptions({
		name: "Little Caesars",
		addonPanelInRight: true
	})
)

configure(loadStories, module)
