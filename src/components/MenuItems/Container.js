import styled from "styled-components"

export default styled("div")`
	display: flex;
	justify-content: space-between;
	border-bottom: 3px solid rgba(50, 50, 50, 1);
	width: 100%;

	h1 {
		color: white;
		margin: 0;
		margin-top: 25px;
	}

	img {
		flex: 1;
		max-width: 140px;
		max-height: 100%;
		object-fit: contain;
	}

	.info {
		ul {
			margin: 0;
			padding: 0;
			list-style-type: none;
		}
	}
`
