import React from "react"
import styled from "styled-components"
import Container from "./Container"

const SubItemContainer = styled("li")`
	.price,
	.name {
		padding-right: 10px;
	}
`

// A component like this one could, presumably, be used across many different Menu Item types.
const SubItem = ({ name, price, calories }) => (
	<SubItemContainer>
		{price && <span className="price">{price}</span>}
		{name && <span className="name">{name}</span>}
		{calories && <span className="calories">{calories} Cal</span>}
	</SubItemContainer>
)

const Idk = ({ item, name }) => {
	const hasSubItems = item.subItems.length > 0

	return (
		<Container>
			<div className="info">
				<h1>{name || item.name}</h1>
				<ul>
					{hasSubItems ? (
						item.subItems.map((subItem, idx) => (
							<SubItem key={idx} calories={subItem.calories} price={subItem.price} name={subItem.name} />
						))
					) : (
						<SubItem calories={item.calories} price={item.price} />
					)}
				</ul>
			</div>

			<img src={item.image} alt={item.name} />
		</Container>
	)
}

export default Idk
