import React from "react"
import styled from "styled-components"
import { MenuItem } from "../../components"

export const Container = styled("div")`
	display: flex;
	background: rgba(237, 112, 54, 1);
	width: 100%;
	height: 100%;
	padding: 20px;

	.left {
		width: 300px;
		border-right: 5px solid white;
	}
`

const slots = {
	leftMiddle: ["L1", "L2", "L3", "L4"]
}

const TemplateLeft = ({ data }) => {
	// use a ref because the index of hte items.leftMiddle isn't an accurate representation of the Meal number
	const totalMeals = React.useRef(0)

	return (
		<Container>
			<div className="left">
				<h1>MAKE IT A MEAL</h1>
				<h2>ADD TO ANY PIZZA</h2>
				<div className="left__meals">
					{slots.leftMiddle.map((id, idx) => {
						if (!data.items[id]) return null

						totalMeals.current++
						// This MenuItem would only work L1-L4 slots (any any other slots that fit the dimensions)
						// Maybe MenU Item components should be created based on their dimension?
						return <MenuItem item={data.items[id]} name={`Meal ${totalMeals.current}`} key={id} />
					})}
				</div>

				{data.items.L5 && (
					<div className="left__footer" style={{ marginTop: 10 }}>
						<h4 style={{ margin: 0 }}>CREATE YOUR OWN</h4>
						<h4 style={{ margin: 0 }}>{data.items.L5.name}</h4>
						<p>Starting at {data.items.L5.price}</p>
						{data.items.L8 && <p>Additional Toppings {data.items.L8.price}</p>}
						{data.items.L6 && (
							<p>
								{data.items.L6.name} {data.items.L6.price} MORE Add ${data.items.L6.calories} Cal
							</p>
						)}
					</div>
				)}
			</div>
		</Container>
	)
}

export default TemplateLeft
