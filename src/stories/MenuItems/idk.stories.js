import React from "react"
import { storiesOf } from "@storybook/react"

import TemplateLeft from "../../components/Templates/Left"

const sampleData = {
	items: {
		L1: {
			name: "ExtraMost Bestest",
			price: "$6",
			calories: "500",
			hnr: "4-8px",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: []
		},
		L8: {
			name: "ExtraMost Bestest",
			price: "$6",
			calories: "500",
			hnr: "4-8px",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: []
		},
		L5: {
			name: "DEEP!DEEP! DISH",
			price: "$2",
			calories: "500",
			hnr: "4-8px",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: []
		},
		L6: {
			name: "Stuffed Crust",
			price: "$2",
			calories: "500",
			hnr: "4-8px",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: []
		},
		L2: {
			name: "ExtraMost Bestest",
			price: "$6",
			hnr: "4-8px",
			calories: "500",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: [
				{
					name: "Pepperoni",
					price: "$5",
					calories: "2210"
				},
				{
					name: "Cheese",
					price: "$5",
					calories: "2210"
				},
				{
					name: "Sausage",
					price: "$5",
					calories: "2210"
				}
			]
		},
		L4: {
			name: "ExtraMost Bestest",
			price: "$6",
			hnr: "4-8px",
			calories: "500",
			image: "https://littlecaesars.com/images/menu-img-classic-pizzas.jpg",
			subItems: [
				{
					name: "Pepperoni",
					price: "$5",
					calories: "2210"
				},
				{
					name: "Cheese",
					price: "$5",
					calories: "2210"
				},
				{
					name: "Sausage",
					price: "$5",
					calories: "2210"
				}
			]
		}
	}
}

storiesOf("Templates", module).add("just a meh template with a badly named MenuItem component", () => {
	return <TemplateLeft data={sampleData} />
})
