import resolve from "rollup-plugin-node-resolve"
import babel from "rollup-plugin-babel"
import postcss from "rollup-plugin-postcss"
import peerDepsExternal from "rollup-plugin-peer-deps-external"
import autoprefixer from "autoprefixer"
import filesize from "rollup-plugin-filesize"
import commonJS from "rollup-plugin-commonjs"

const external = ["react", "react-dom", "prop-types", "styled-components"]

export default {
	input: "./src/components/index.js",
	output: {
		format: "es",
		file: "./dist/index.js",
		name: "IWReactComponents"
	},
	plugins: [
		peerDepsExternal(),
		babel(),
		resolve(),
		commonJS({
			include: "node_modules/**"
		}),
		postcss({ extract: true, plugins: [autoprefixer] }),
		filesize()
	],
	external
}
